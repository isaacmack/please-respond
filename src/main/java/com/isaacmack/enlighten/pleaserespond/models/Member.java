package com.isaacmack.enlighten.pleaserespond.models;

public class Member {
	public Long member_id;
	public String photo;
	public String member_name;
	
	public Member(Long member_id, String photo, String member_name) {
		this.member_id = member_id;
		this.photo = photo;
		this.member_name = member_name;
	}
}
