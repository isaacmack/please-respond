package com.isaacmack.enlighten.pleaserespond.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;

/**
 * Class to generate the final report that will be output to the terminal
 * 
 * @author isaac
 *
 */
public class ReportOutput {
	
	public int number_rsvp_received;
	public Date latest_event_date;
	public String latest_event_url;
	public Map<String,Long> top3_country_vs_count_rsvps;
	
	
	/**
	 * Constructor
	 * 
	 * @param number_rsvp_received the number of rsvps received
	 * @param latest_event_date the latest date retrieved
	 * @param latest_event_url the latest event url
	 * @param top3_country_vs_count_rsvps a map of countries and how many times they occurred
	 */
	public ReportOutput(int number_rsvp_received, Date latest_event_date, String latest_event_url,
			Map<String, Long> top3_country_vs_count_rsvps) {
		this.number_rsvp_received = number_rsvp_received;
		this.latest_event_date = latest_event_date;
		this.latest_event_url = latest_event_url;
		this.top3_country_vs_count_rsvps = top3_country_vs_count_rsvps;
	}
	
	
//	@Override
//	public String toString() {
//		return new Gson().toJson(this);
//	}
	
	
	/**
	 * Generate the output for this report object
	 * 
	 * @return the string to be printed to the terminal
	 */
	public String generateOutput() {
		
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String datestring = "";
		
		if(latest_event_date != null) {
			try {
				datestring = sdf.format(latest_event_date);
			}
			catch(NumberFormatException e) {
				LoggerFactory.getLogger(ReportOutput.class).warn(e.getMessage());
			}
		}
		
		
		/**
		 * Sorts the countries with their counts by the largest count first and
		 * joins them with commas to form:
		 * 
		 * us,29,ca,20 (etc)
		 * 
		 * Doesn't care about how many there are.
		 * 
		 */
		final String country_counts = String.join(",", top3_country_vs_count_rsvps.entrySet().stream()
			.sorted((e1,e2) -> { return -1 * e1.getValue().compareTo(e2.getValue()); }) // sort by largest first
			.map( (e) -> { 
				
				// take a given key+val and return it as a string in the form:
				// key,val
				return String.join(",", new String[] { 
				  		  e.getKey()
						, Long.toString(e.getValue()) 
				}); 
			})
			.collect(Collectors.toList())
			)
			;
		
		
		// the final result
		return String.join(",", new String[] {
				  Integer.toString(number_rsvp_received)
				, datestring
				, nvl(latest_event_url)
				, country_counts
		});
	}
	
	
	/**
	 * Convenience/util method to change nulls into empty space for report
	 * output.
	 * 
	 * @param in the original string (or null)
	 * @return the original string or empty string if input was null
	 */
	private String nvl(String in) {
		return in == null ? "" : in;
	}
	
}
