package com.isaacmack.enlighten.pleaserespond.models;

import com.google.gson.Gson;

/**
 * POJO for a single line item returned from the meetup.com stream
 * 
 * @author isaac
 *
 */
public class MeetupStreamPayloadItem {
	public Venue venue;
	public String visibility;
	public String response;
	public Long guests;
	public Member member;
	public Long rsvp_id;
	public Long mtime;
	public Event event;
	public Group group;
	
	public MeetupStreamPayloadItem(Venue venue, String visibility, String response, Long guests, Member member,
			Long rsvp_id, Long mtime, Event event, Group group) {
		this.venue = venue;
		this.visibility = visibility;
		this.response = response;
		this.guests = guests;
		this.member = member;
		this.rsvp_id = rsvp_id;
		this.mtime = mtime;
		this.event = event;
		this.group = group;
	}
	
	
	/**
	 * Convenience for dev/testing
	 */
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
