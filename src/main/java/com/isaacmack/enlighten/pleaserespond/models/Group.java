package com.isaacmack.enlighten.pleaserespond.models;

import java.util.List;

public class Group {
	public List<GroupTopic> group_topics;
	public String group_city;
	public String group_country;
	public Long group_id;
	public String group_name;
	public Double group_lon;
	public String group_urlname;
	public String group_state;
	public Double group_lat;
	
	
	public Group(List<GroupTopic> group_topics, String group_city, String group_country, Long group_id,
			String group_name, Double group_lon, String group_urlname, String group_state, Double group_lat) {
		this.group_topics = group_topics;
		this.group_city = group_city;
		this.group_country = group_country;
		this.group_id = group_id;
		this.group_name = group_name;
		this.group_lon = group_lon;
		this.group_urlname = group_urlname;
		this.group_state = group_state;
		this.group_lat = group_lat;
	}
	
}
