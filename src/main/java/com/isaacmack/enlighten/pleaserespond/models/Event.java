package com.isaacmack.enlighten.pleaserespond.models;

public class Event {
	public String event_name;
	public String event_id;
	public Long time;
	public String event_url;
	
	public Event(String event_name, String event_id, Long time, String event_url) {
		this.event_name = event_name;
		this.event_id = event_id;
		this.time = time;
		this.event_url = event_url;
	}
}
