package com.isaacmack.enlighten.pleaserespond.models;

public class Venue {
	public String venue_name;
	public double lon;
	public double lat;
	public long venue_id;
	
	public Venue(String venue_name, double lon, double lat, long venue_id) {
		this.venue_name = venue_name;
		this.lon = lon;
		this.lat = lat;
		this.venue_id = venue_id;
	}
}
