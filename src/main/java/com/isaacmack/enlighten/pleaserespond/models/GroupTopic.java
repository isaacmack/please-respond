package com.isaacmack.enlighten.pleaserespond.models;

public class GroupTopic {
	public String urlkey;
	public String topic_name;
	
	public GroupTopic(String urlkey, String topic_name) {
		this.urlkey = urlkey;
		this.topic_name = topic_name;
	}
}
