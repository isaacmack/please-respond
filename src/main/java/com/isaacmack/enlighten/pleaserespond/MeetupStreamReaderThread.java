package com.isaacmack.enlighten.pleaserespond;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.isaacmack.enlighten.pleaserespond.models.MeetupStreamPayloadItem;

/**
 * Thread that pulls rsvp data from the provided meetup.com stream
 * 
 * @author isaac
 *
 */
public class MeetupStreamReaderThread extends Thread {

	private final Logger logger;
	private final String meetup_stream_url;
	private final int listen_secs;
	
	private final Queue<MeetupStreamPayloadItem> queue;
	
	
	/**
	 * Constructor. No auto-start
	 * 
	 * Sets daemon, because like I said elsewhere I originally envisioned that
	 * this would be continually running for the life of the program.
	 * 
	 * @param queue the queue to place retrieved/incomming rsvps
	 * @param meetup_url the URL of the meetup.com stream
	 * @param listen_secs the number of seconds to listen to the stream
	 */
	public MeetupStreamReaderThread(Queue queue, String meetup_url, int listen_secs) {
		this.setDaemon(true); // terminate with the rest of the program
		logger = LoggerFactory.getLogger(this.getClass());
		
		this.meetup_stream_url = meetup_url;
		this.listen_secs = listen_secs;
		this.queue = queue;
	}

	@Override
	public void run() {
		
		try {
			readFromStream(meetup_stream_url,listen_secs);
		} catch (Exception e) {
			logger.error("Error occurred while reading from the stream => " + e.getMessage());
		}
		
	}
	
	
	/**
	 * Read from the meetup.com stream and translate into objects in queue
	 * 
	 * @param meetup_stream_url
	 * @param listen_secs
	 * @throws IOException when an error occurs with the stream
	 * @throws Exception when HTTP request returns not 200,301,302
	 */
	private void readFromStream(String meetup_stream_url, int listen_secs) throws Exception {
		final URL url = new URL(meetup_stream_url);
		final HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setInstanceFollowRedirects(true);
		
		
		int status = conn.getResponseCode();
		switch(status) {
		
			case 200:
				break;
				
			// follow redirects (stack!)
			case 301:
			case 302:
				final String location = conn.getHeaderField("Location");
				readFromStream(location, listen_secs);
				return;
				
				
			default:
				throw new Exception("MeetupStreamReaderThread encountered an HTTP error " + status);
		
		}
		
		final long starttime = System.currentTimeMillis();
		
		try(final InputStream is = conn.getInputStream()) { // autoclose stream
		
			final BufferedReader br = new BufferedReader(new InputStreamReader(is));
			
			final Gson gson = new Gson();
			String line = "";
			
			while((line = br.readLine()) != null) {
				
				logger.debug(line);
				// using Gson to translate JSON payload to POJO
				
				try {
					final MeetupStreamPayloadItem mspi = gson.fromJson(line, MeetupStreamPayloadItem.class);
					queue.add(mspi);
				}
				catch(JsonParseException e) {
					logger.error("Error reading JSON data from stream. Skipping.");
				}
				
				
				final long now = System.currentTimeMillis();
				
				// leave the loop if we've been reading for the requisite time
				if(now - starttime > (listen_secs * 1000))
					break;
			}
			
			logger.debug("Finished reading stream. Time: " + listen_secs + " seconds.");
		
		}
	}
	
}
