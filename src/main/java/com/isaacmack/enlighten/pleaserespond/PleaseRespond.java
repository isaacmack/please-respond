package com.isaacmack.enlighten.pleaserespond;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isaacmack.enlighten.pleaserespond.models.Event;
import com.isaacmack.enlighten.pleaserespond.models.MeetupStreamPayloadItem;
import com.isaacmack.enlighten.pleaserespond.models.ReportOutput;

/**
 * Entry class. Pulls streaming rsvp data from meetup.com and generates a
 * simple report about how many items were read, details about the latest
 * event, and which countries had the most rsvps.
 * 
 * @author isaac
 *
 */
public class PleaseRespond {
	
	private final static int DEFAULT_LISTEN_SECONDS = 60;
	
	private Logger logger;
	
	
	/**
	 * Constructor. Starts thread to read from meetup.com stream and retrieves
	 * results, then outputs final report from those results.
	 * 
	 * @param meetup_url the URL of the meetup.com rsvp streaming service
	 * @param listen_secs the number of seconds to collect rsvp data
	 * @throws InterruptedException if someone interrupts the thread
	 * @throws IOException if there is an issue reading from the rsvp streaming service
	 */
	private PleaseRespond(String meetup_url, int listen_secs) throws InterruptedException,IOException {
		
		logger = LoggerFactory.getLogger(PleaseRespond.class);
		logger.debug("Starting");
		
		// LinkedBlockingQueue is thread safe
		final LinkedBlockingQueue<MeetupStreamPayloadItem> queue = new LinkedBlockingQueue<>();
		
		
		/**
		 * Running this in a thread is completely unnecessary. At first I was
		 * thinking that I would be constantly reading from the stream and
		 * periodically need to generate reports. Hypothetically, I could have
		 * set a size limit to the queue/dequeue and held on to a limited
		 * number of items, periodically generating the desired output every
		 * few minutes or on request.
		 * 
		 * Additionally, I could add code here to ensure that the thread only
		 * runs for a certain number of seconds. Let's say for example the 
		 * meetup.com stops streaming data for some reason. The thread could 
		 * wait far beyond its configured read time leaving the program hung.
		 * By replacing the below join() with a sleeping loop that will send an
		 * interrupt to the thread after the time allotted has expired we can 
		 * ensure the thread will not run longer than desired.
		 * 
		 */
		final Thread t = new MeetupStreamReaderThread(queue, meetup_url, listen_secs);
		t.start();
		t.join();
		
		
		/**
		 * //example with Thread.interrupt()
		 * 
		 * final long starttime = System.currentTimeMillis();
		 * 
		 * while(t.isAlive()) {
		 * 		
		 * 		final long now = System.currentTimeMillis();
		 * 
		 * 		if(now - starttime > (listen_secs * 1000)) {
		 * 			t.interrupt();
		 * 			break;
		 * 		}
		 * 
		 * 		Thread.sleep(5*1000); // sleep 5s
		 * }
		 * 
		 */
		
		
		logger.debug(queue.size() + " total rsvp data items");
		
		
		System.out.println(generateReport(queue).generateOutput());
		
		
		logger.debug("Finished");
	}
	
	
	
	/**
	 * From all rsvps, get the latest event
	 * 
	 * @param queue
	 * @return
	 */
	private Optional<Event> getLatestEvent(Queue<MeetupStreamPayloadItem> queue) {
		return queue.stream()
				.map( (mspi) -> { return mspi.event; }) // retrieve the event
				.max( (e1,e2) -> { // sort events by time 
					return e1.time.compareTo(e2.time); // sort by greatest time first
				})
				;
	}
	
	
	
	/**
	 * Takes the accumulated data and builds a report object from them
	 * 
	 * @param queue the items received from the rsvp meetup.com streaming service
	 * @return the built report object
	 */
	private ReportOutput generateReport(Queue<MeetupStreamPayloadItem> queue) {
		
		final int number_rsvp_received = queue.size();
		
		final Optional<Event> latest_event = getLatestEvent(queue);
		
		
		Date latest_event_date = null;
		String latest_event_url = null;
		if(latest_event.isPresent()) {
			latest_event_date = new Date(latest_event.get().time);
			latest_event_url = latest_event.get().event_url;
		}
		
		
		final Map<String,Long> top3_country_vs_count_rsvps = get_top3_country_counts(queue);
		
		
		return new ReportOutput(number_rsvp_received, latest_event_date, latest_event_url, top3_country_vs_count_rsvps);
		
	}
	
	
	/**
	 * Returns a map of country to the number of times an rsvp for that country
	 * appears in the stream. Top 3 countries only.
	 * 
	 * @param queue
	 * @return
	 */
	private Map<String,Long> get_top3_country_counts(Queue<MeetupStreamPayloadItem> queue) {
		
		/**
		 * First generate map of country to the number of times it occurs:
		 * 
		 * ie: {"us":20,"ca":18} etc
		 * 
		 */
		final Map<String,Long> country_counts = queue.stream()
				.map( (mspi) -> { return mspi.group.group_country; }) // countries
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
				;
		
		
		
		/**
		 * Example alternative for the above:
		 * 
		 * final Map<String,Long> country_counts = new HashMap<>();
				queue.stream()
					.map( (mspi) -> { return mspi.group.group_country; })
					.forEach( (country) -> {
						Long val = country_counts.get(country);
						if(val == null)val = 0l;
						val++;
						country_counts.put(country,val);
					}); 
		 * 
		 */
		
			
		
		// full list of countries with their counts
		logger.debug(new Gson().toJson(country_counts));
		
		
		/**
		 * find the 3 countries that occur the most number of times
		 * 
		 * problem: if the last 2 or 3 countries have the same counts, ie
		 * {"us":52,"ca":5,"jp":5,"kr":5}
		 * then which two out of the three with 5 rsvps returned will be
		 * undefined. 
		 * 
		 */
		final Map<String,Long> top3_country_vs_count_rsvps =
				country_counts.entrySet().stream()
				.sorted( (e1,e2) -> { return -1 * e1.getValue().compareTo(e2.getValue()); } ) // sort by largest occurences
				.limit(3)
				.collect(Collectors.toMap((e) -> { return e.getKey(); }, (e) -> { return e.getValue(); }) )
				;
		
		return top3_country_vs_count_rsvps;
	}
	
	
	/**
	 * Program entry point.
	 * 
	 * Checks for listening time argument passed to
	 * program, falling back to config.properties value, falling back to
	 * hardcoded value.
	 * 
	 * Checks config.properties for meetup.com stream url and exits(1) if it
	 * isn't set.
	 * 
	 * @param args
	 * @throws IOException if config.properties doesn't exist
	 * @throws InterruptedException if thread that pulls rsvp data is interrupted
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		
		final Logger logger = LoggerFactory.getLogger(PleaseRespond.class);
		
		final Properties p = new Properties();
		p.load(new FileInputStream("config.properties"));
		
		final String meetup_url = p.getProperty("MEETUP_STREAM_URL");
		
		if(meetup_url == null) {
			logger.error("config.properties is missing MEETUP_STREAM_URL");
			System.exit(1);
		}
		
		
		int listen_secs = DEFAULT_LISTEN_SECONDS;
		
		if(args.length == 1) {
			try {
				listen_secs = Integer.parseInt(args[0]);
			}
			catch(NumberFormatException e) {
				logger.warn("Invalid argument for listening time.");
				printUsage();
				System.exit(1);
			}
		}
		else {
			final String s_listen_secs = p.getProperty("DEFAULT_LISTEN_TIME");
			
			if(s_listen_secs != null) {
				try {
					listen_secs = Integer.parseInt(s_listen_secs);
				}
				catch(NumberFormatException e) {
					logger.warn("config.properties has invalid or missing DEFAULT_LISTEN_TIME");
				}
			}
		}
		
		
		new PleaseRespond(meetup_url, listen_secs);
	}
	
	
	private static void printUsage() {
		System.err.println("Usage: " + PleaseRespond.class.getSimpleName() + " [seconds_to_listen]");
	}
	
}